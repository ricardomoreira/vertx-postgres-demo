## VERTX POSTGRES DEMO

This is a demo version of a vert.x app based on Clement Escoffier [post](https://developers.redhat.com/blog/2018/04/18/eclipse-vertx-reactive-extensions) about introduction to Vert.x

## Dependencies
In order to install and run this app, according to these instructions, the OS must have installed the following dependencies:

* [Git](https://git-scm.com/)
* [PostgreSQL](https://www.postgresql.org/)
* [Maven](https://maven.apache.org/)

## Installation

After ensuring that the operating system has all the dependencies follow these steps:

### Clone repo

Open a terminal and place the command prompt on the folder where the project will be installed (e.g.: "cd ~/my-projects") and run the command:

``` bash
# clone the repository
$ git clone https://gitlab.com/ricardomoreira/vertx-postgres-demo.git
```

### Add configuration

There are several ways of passing configuration parameters to a Vert.x app. This one relies on the config.json file
placed under [app root]/src/main/conf/ folder.

A configuration file example is provided. Copy it and update it according to local settings. From the project root 
folder:

``` bash
# copy config file
$ cp ./src/main/conf/config.json.dist ./src/main/conf/config.json
```

Update config.json according to local settings.

### Create database

Create the database that will be used by the app. The name of the database must be the onse set on database connection url string ("my_read_list" on config.json.dist case). 

### Run migrations

``` bash
# run migrations
$ mvn flyway:migrate
```

## Usage

From the root folder of the project directory, run command:

``` bash
# compile and run on default port (8082)
$ mvn compile vertx:run
```

App now is serving these pages:

* [Hello Page](http://localhost:8082)
* [Homepage](http://localhost:8082/assets/index.html in the browser)