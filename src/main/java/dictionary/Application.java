package dictionary;

import dictionary.controller.TermsController;
import dictionary.service.DBClient;
import dictionary.service.LanguageService;
import dictionary.service.TermService;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.CompletableHelper;
import io.vertx.reactivex.config.ConfigRetriever;
import io.vertx.reactivex.core.AbstractVerticle;
import io.vertx.reactivex.core.Vertx;
import io.vertx.reactivex.core.http.HttpServerResponse;
import io.vertx.reactivex.ext.jdbc.JDBCClient;
import io.vertx.reactivex.ext.web.Router;

public class Application extends AbstractVerticle {

    private JsonObject config;
    private DBClient dbClient;
    private JDBCClient jdbc;
    private LanguageService languageService;
    private TermService termService;
    private TermsController controller;
    private Router router;

    @Override
    public void start(Future<Void> fut) {

        ConfigRetriever retriever = ConfigRetriever.create(vertx);

        retriever.rxGetConfig()
                .flatMap(config -> {
                    this.config = config;
                    jdbc = JDBCClient.createShared(vertx, this.config, "dictionary");

                    dbClient = new DBClient(jdbc);
                    termService = new TermService(dbClient);
                    languageService = new LanguageService(dbClient);
                    controller = new TermsController(termService, languageService);

                    // set routes
                    router = Router.router(vertx);
                    router.route("/").handler(routingContext -> {
                        HttpServerResponse response = routingContext.response();
                        response
                                .putHeader("content-type", "text/html")
                                .end("<h1>Hello from Dictionary</h1>");
                    });
                    router.get("/api/dictionary/terms").handler(controller::getAll);
                    router.get("/api/dictionary/terms/:id").handler(controller::getOne);
                    router.get("/api/dictionary/terms/:id/translations").handler(controller::getAllTranslations);

                    return Single.just(router);
                })
                .flatMapCompletable(router -> createHttpServer(this.config, router, vertx))
                .subscribe(CompletableHelper.toObserver(fut));
    }

    private Completable createHttpServer(JsonObject config, Router router, Vertx vertx) {
        return vertx
                .createHttpServer()
                .requestHandler(router::accept)
                .rxListen(config.getInteger("HTTP_PORT", 8080))
                .toCompletable();
    }
}