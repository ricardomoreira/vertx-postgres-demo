package dictionary.model;

import io.vertx.core.json.JsonObject;

public class Language {

    private long id = -1;

    private String locale;

    private String displayName;

    private String country;

    private String script;

    public Language(String locale, String displayName, String country, String script) {
        this.locale = locale;
        this.displayName = displayName;
        this.country = country;
        this.script = script;
    }

    public Language(long id, String locale, String displayName, String country, String script) {
        this.id = id;
        this.locale = locale;
        this.displayName = displayName;
        this.country = country;
        this.script = script;
    }

    public Language() {

    }

    public Language(JsonObject json) {
        this(
                json.getInteger("id", -1),
                json.getString("locale"),
                json.getString("display_name"),
                json.getString("display_country"),
                json.getString("script")
        );
    }

    public long getId() {
        return id;
    }

    public String getLocale() {
        return locale;
    }

    public Language setLocale(String locale) {
        this.locale = locale;
        return this;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Language setDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public Language setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getScript() {
        return script;
    }

    public Language setScript(String script) {
        this.script = script;
        return this;
    }

}
