package dictionary.model;

import io.vertx.core.json.JsonObject;

public class Term {

    private long id = -1;

    public Term(long id) {
        this.id = id;
    }

    public Term() {

    }

    public Term(JsonObject json) {
        this(
                json.getInteger("id", -1)
        );
    }

    public long getId() {
        return id;
    }
}
