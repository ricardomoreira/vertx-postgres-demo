package dictionary.model;

import io.vertx.core.json.JsonObject;

public class TermTrans {

    private long id = -1;

    private long idTerm;

    private String name;

    private String definition;

    private long idLanguage;

    public TermTrans(String name, String definition, long idLanguage, long idTerm) {
        this.name = name;
        this.definition = definition;
        this.idLanguage = idLanguage;
        this.idTerm = idTerm;
    }

    public TermTrans(long id, String name, String definition, long idLanguage, long idTerm) {
        this.id = id;
        this.name = name;
        this.definition = definition;
        this.idLanguage = idLanguage;
        this.idTerm = idTerm;
    }

    public TermTrans() {

    }

    public TermTrans(JsonObject json) {
        this(
                json.getInteger("id", -1),
                json.getString("name"),
                json.getString("definition"),
                json.getInteger("id_lang"),
                json.getInteger("id_term")
        );
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public TermTrans setName(String name) {
        this.name = name;
        return this;
    }

    public String getDefinition() {
        return definition;
    }

    public TermTrans setDefinition(String definition) {
        this.definition = definition;
        return this;
    }

    public long getIdLanguage() {
        return idLanguage;
    }

    public TermTrans setIdLanguage(long idLanguage) {
        this.idLanguage = idLanguage;
        return this;
    }

    public long getIdTerm() {
        return idTerm;
    }

    public TermTrans setIdTerm(long idTerm) {
        this.idTerm = idTerm;
        return this;
    }
}
