package dictionary.controller;

import dictionary.service.LanguageService;
import dictionary.service.TermService;
import io.vertx.core.json.Json;
import io.vertx.reactivex.ext.web.RoutingContext;

public class TermsController {

    private final String DEFAULT_LOCALE = "en";

    private TermService termService;
    private LanguageService langService;

    public TermsController(TermService termService, LanguageService langService) {
        this.termService = termService;
        this.langService = langService;

    }

    private Long getLocale(RoutingContext rc) {
        String locale = rc.request().getHeader("locale");

        Long[] localeId = new Long[]{1L};

        if (locale == null || locale.length() == 0) return localeId[0];

        langService.getOne(locale).subscribe(id -> {
            if (id == -1L) {
                localeId[0] = 1L;
            } else {
                localeId[0] = id;
            }
        });

        return localeId[0];
    }

    public void getAll(RoutingContext rc) {
        Long locale = getLocale(rc);
        termService.getAll(locale)
                .subscribe(
                        terms->{
                            rc.response().setStatusCode(200)
                                    .putHeader("content-type", "application/json; charset=utf-8")
                                    .end(Json.encodePrettily(terms));
                        },
                        err->{
                            rc.response().setStatusCode(500).end(Json.encodePrettily(err.getMessage()));
                        }
                );
    }

    public void getAllTranslations(RoutingContext rc) {
        String id = rc.pathParam("id");
        termService.getAllTranslations(id)
                .subscribe(
                        terms->{
                            rc.response().setStatusCode(200)
                                    .putHeader("content-type", "application/json; charset=utf-8")
                                    .end(Json.encodePrettily(terms));
                        },
                        err->{
                            rc.response().setStatusCode(500).end(Json.encodePrettily(err.getMessage()));
                        }
                );
    }

    public void getOne(RoutingContext rc) {
        Long locale = getLocale(rc);
        String id = rc.pathParam("id");
        termService.getOne(id, locale)
                .subscribe(
                        term->{
                            rc.response().setStatusCode(200)
                                    .putHeader("content-type", "application/json; charset=utf-8")
                                    .end(Json.encodePrettily(term));
                        },
                        err->{
                            rc.response().setStatusCode(500).end(Json.encodePrettily(err.getMessage()));
                        }
                );
    }

}
