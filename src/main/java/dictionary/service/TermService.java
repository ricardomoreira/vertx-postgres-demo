package dictionary.service;

import dictionary.model.TermTrans;
import io.reactivex.Single;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public class TermService {

    private DBClient cli;

    public TermService(DBClient cli) {
        this.cli = cli;
    }

    public Single<List<TermTrans>> getAll(Long locale) {
        String sql = "SELECT * FROM term_trans WHERE id_lang = ?";
        return cli.connect().flatMap(connection->connection.rxQueryWithParams(sql, new JsonArray().add(locale))
                .map(rs->rs.getRows().stream().map(TermTrans::new).collect(Collectors.toList()))
                .doFinally(connection::close));
    }

    public Single<TermTrans> getOne(String id, Long locale) {
        String sql = "SELECT * FROM term_trans WHERE id_term = ? AND id_lang = ?";
        return cli.connect().flatMap(connection->connection.rxQueryWithParams(sql, new JsonArray().add(Integer.valueOf(id)).add(locale))
                .doFinally(connection::close)
                .map(rs->{
                    List<JsonObject> rows = rs.getRows();
                    if (rows.size() == 0) {
                        throw new NoSuchElementException("No term with id " + id);
                    } else {
                        JsonObject row = rows.get(0);
                        return new TermTrans(row);
                    }
                }));
    }

    public Single<List<TermTrans>> getAllTranslations(String id) {
        String sql = "SELECT * FROM term_trans WHERE id_term = ?";
        return cli.connect().flatMap(connection->connection.rxQueryWithParams(sql, new JsonArray().add(id))
                .map(rs->rs.getRows().stream().map(TermTrans::new).collect(Collectors.toList()))
                .doFinally(connection::close));
    }

}


