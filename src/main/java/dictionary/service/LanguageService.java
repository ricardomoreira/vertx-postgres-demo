package dictionary.service;

import dictionary.model.Language;
import io.reactivex.Single;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.util.List;

public class LanguageService {

    private DBClient cli;

    public LanguageService(DBClient cli) {
        this.cli = cli;
    }

    public Single<Long> getOne(String locale) {
        String sql = "SELECT * FROM language WHERE locale = ?";
        return cli.connect()
                .flatMap(connection -> connection.rxQueryWithParams(sql, new JsonArray().add(locale))
                .doFinally(connection::close)
                .map(rs->{
                    List<JsonObject> rows = rs.getRows();
                    if (rows.size() == 0) {
                        // TODO LOG
                        return Long.valueOf(-1);
                    } else {
                        JsonObject row = rows.get(0);
                        return new Language(row).getId();
                    }
                }));
    }
}


