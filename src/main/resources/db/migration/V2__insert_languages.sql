INSERT INTO language (locale, display_name, country, script) VALUES
    ('en', 'English Default', 'United Kingdom', 'Latin'),
    ('pt', 'Portuguese Default', 'Portugal', 'Latin'),
    ('es', 'Spanish Default', 'Spain', 'Latin');
