CREATE TABLE term (id SERIAL PRIMARY KEY);

CREATE TABLE language (id SERIAL PRIMARY KEY, locale VARCHAR(20), display_name TEXT, country TEXT, script VARCHAR(20));

CREATE TABLE term_trans (id SERIAL PRIMARY KEY, id_term INTEGER REFERENCES term(id), id_lang INTEGER REFERENCES language(id), name TEXT NOT NULL, definition TEXT);